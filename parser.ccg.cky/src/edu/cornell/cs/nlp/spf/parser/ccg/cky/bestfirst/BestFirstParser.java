package edu.cornell.cs.nlp.spf.parser.ccg.cky.bestfirst;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Predicate;

import edu.cornell.cs.nlp.spf.ccg.categories.Category;
import edu.cornell.cs.nlp.spf.ccg.categories.ICategoryServices;
import edu.cornell.cs.nlp.spf.ccg.lexicon.ILexiconImmutable;
import edu.cornell.cs.nlp.spf.data.sentence.Sentence;
import edu.cornell.cs.nlp.spf.parser.ISentenceLexiconGenerator;
import edu.cornell.cs.nlp.spf.parser.ParsingOp;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.AbstractCKYParser;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.CKYBinaryParsingRule;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.CKYUnaryParsingRule;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.chart.AbstractCellFactory;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.chart.Cell;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.chart.Chart;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.single.CKYParser;
import edu.cornell.cs.nlp.spf.parser.ccg.cky.single.CKYParser.Builder;
import edu.cornell.cs.nlp.spf.parser.ccg.model.IDataItemModel;
import edu.cornell.cs.nlp.spf.parser.ccg.rules.ILexicalRule;
import edu.cornell.cs.nlp.spf.parser.ccg.rules.LexicalRule;
import edu.cornell.cs.nlp.utils.composites.Pair;
import edu.cornell.cs.nlp.utils.filter.FilterUtils;
import edu.cornell.cs.nlp.utils.filter.IFilter;

public class BestFirstParser<DI extends Sentence, MR>
        extends AbstractCKYParser<DI, MR> {

	private static final long serialVersionUID = 1L;

	protected BestFirstParser(
			int beamSize, 
			CKYBinaryParsingRule<MR>[] binaryRules,
			List<ISentenceLexiconGenerator<DI, MR>> sentenceLexiconGenerators,
			List<ISentenceLexiconGenerator<DI, MR>> sloppyLexicalGenerators, 
			ICategoryServices<MR> categoryServices,
			boolean pruneLexicalCells, 
			IFilter<Category<MR>> completeParseFilter, 
			CKYUnaryParsingRule<MR>[] unaryRules,
			ILexicalRule<MR> lexicalRule, 
			boolean breakTies) {
		
		super(
				beamSize, 
				binaryRules, 
				sentenceLexiconGenerators, 
				sloppyLexicalGenerators, 
				categoryServices, 
				pruneLexicalCells,
				completeParseFilter, 
				unaryRules, 
				lexicalRule, 
				breakTies
			);
	}
	
	
	/**
	 * @param chart parsing chart to examine
	 * @return whether {@code chart} contains a complete parse
	 */
	protected boolean containsCompleteParse(Chart<MR> chart) {
		int sentenceLength = chart.getSentenceLength();
		for(Cell<MR> cell : chart.getSpanIterable(0, sentenceLength - 1)) {
			if (cell.isFullParse()) return true;
		}
		return false;		
	}

	@Override
	protected Chart<MR> doParse(
			Predicate<ParsingOp<MR>> pruningFilter, 
			IDataItemModel<MR> model,
			Chart<MR> chart, 
			int numTokens, 
			AbstractCellFactory<MR> cellFactory, 
			ILexiconImmutable<MR> lexicon) {
		
		final int sentenceLength = chart.getSentenceLength();
		
		Agenda<MR> agenda = new Agenda<>(new CellComparator<MR>());
		

		// Add lexical entries from all active lexicons
		for (int start = 0; start < numTokens; start++) {
			for (int end = start; end < numTokens; end++) {
				
				final Pair<Collection<Cell<MR>>, Boolean> processingPair = 
					generateLexicalCells(						
						start, 
						end, 
						chart, 
						lexicon, 
						model, 
						pruningFilter
					);				
				agenda.addAll(processingPair.first());
				
				// Apply unary rules to cells added by lexical entries.
				final Pair<List<Cell<MR>>, Boolean> unaryProcessingResult = 
					unaryProcessSpan(
						start, 
						end, 
						sentenceLength, 
						chart, 
						cellFactory,
						pruningFilter,
						model
					);				
				agenda.addAll(unaryProcessingResult.first());
			}
		}
		
		while(!agenda.isEmpty() && !containsCompleteParse(chart)) {

			
			/* TODO does not currently do externalPruning()
			 * Also does not handle where viterbiScore for a cell is updated
			 * PriorityQueue is not thread safe
			 */
			
			Cell<MR> cell = agenda.poll();

			
			chart.add(cell);
			
			int start = cell.getStart();
			int end = cell.getEnd();
			
			/* Try to combine newly inserted cell with cells spanning from
			 * leftBoundary to (start - 1)
			 */
			for(int leftBoundary = 0; leftBoundary < start; leftBoundary++){
				Pair<List<Cell<MR>>, Boolean> processingPair = 
					processSplit(
						leftBoundary,
						end,
						start - 1 - leftBoundary,
						sentenceLength,
						chart,
						cellFactory,
						pruningFilter,
						model
					);
				
				agenda.addAll(processingPair.first());
			}
			
			/*
			 * Try to combine newly inserted cell with cells spanning from
			 * (end + 1) to rightBoundary
			 */
			for(int rightBoundary = end + 1; 
					rightBoundary < sentenceLength; 
					rightBoundary++) {
				Pair<List<Cell<MR>>, Boolean> processingPair = 
					processSplit(
						start,
						rightBoundary,
						end - start,
						sentenceLength,
						chart,
						cellFactory,
						pruningFilter,
						model
					);
					
				agenda.addAll(processingPair.first());
			}
			
			Pair<List<Cell<MR>>, Boolean> unaryProcessingPair =
				unaryProcessSpan(
					start,
					end,
					sentenceLength,
					chart,
					cellFactory,
					pruningFilter,
					model
				);
			
			agenda.addAll(unaryProcessingPair.first());			
		}
		
		return chart;		
	}
	
	
	/**
	 * A comparator class for comparing {@code Cell<MR>} instances by descending 
	 * Viterbi score.
	 *
	 * @param <MR> meaning representation
	 */
	protected static class CellComparator<MR> implements Comparator<Cell<MR>> {

		@Override
		public int compare(Cell<MR> o1, Cell<MR> o2) {
			/*
			 * We want cell with higher Viterbi scores to appear first in the
			 * priority queue, thus cells with higher Viterbi scores should be
			 * "less" than cells with lower Viterbi scores
			 */
			double diff = o2.getViterbiScore() - o1.getViterbiScore();
			return (diff < 0) ? -1 : 
				   (diff > 0) ? 1  :
			       0;
		}
		
	}

	
	/**
	 * Builder for {@link BestFirstParser}.
	 */
	public static class Builder<DI extends Sentence, MR> {

		private final Set<CKYBinaryParsingRule<MR>>	binaryRules = 
				new HashSet<CKYBinaryParsingRule<MR>>();

		private boolean	breakTies = false;

		private final ICategoryServices<MR> categoryServices;

		private IFilter<Category<MR>> completeParseFilter = 
				FilterUtils.stubTrue();

		private ILexicalRule<MR> lexicalRule = new LexicalRule<MR>();

		/** The maximum number of cells allowed in each span */
		private int maxNumberOfCellsInSpan = 50;

		private boolean pruneLexicalCells = false;

		private final List<ISentenceLexiconGenerator<DI, MR>> 
			sentenceLexicalGenerators = 
				new ArrayList<ISentenceLexiconGenerator<DI, MR>>();

		private final List<ISentenceLexiconGenerator<DI, MR>>
			sloppyLexicalGenerators = 
				new ArrayList<ISentenceLexiconGenerator<DI, MR>>();

		private final Set<CKYUnaryParsingRule<MR>> unaryRules = 
				new HashSet<CKYUnaryParsingRule<MR>>();

		
		public Builder(ICategoryServices<MR> categoryServices) {
			this.categoryServices = categoryServices;
		}

		public Builder<DI, MR> addParseRule(CKYBinaryParsingRule<MR> rule) {
			binaryRules.add(rule);
			return this;
		}

		public Builder<DI, MR> addParseRule(CKYUnaryParsingRule<MR> rule) {
			unaryRules.add(rule);
			return this;
		}

		public Builder<DI, MR> addSentenceLexicalGenerator(
				ISentenceLexiconGenerator<DI, MR> generator) {
			sentenceLexicalGenerators.add(generator);
			return this;
		}

		public Builder<DI, MR> addSloppyLexicalGenerator(
				ISentenceLexiconGenerator<DI, MR> sloppyGenerator) {
			sloppyLexicalGenerators.add(sloppyGenerator);
			return this;
		}

		@SuppressWarnings("unchecked")
		public BestFirstParser<DI, MR> build() {
			CKYBinaryParsingRule<MR>[] binaryRulesArray =
					new CKYBinaryParsingRule[binaryRules.size()];			
			binaryRulesArray = binaryRules.toArray(binaryRulesArray);

			CKYUnaryParsingRule<MR>[] unaryRulesArray =
					new CKYUnaryParsingRule[unaryRules.size()];
			unaryRulesArray = unaryRules.toArray(unaryRulesArray);			
			
			return new BestFirstParser<DI, MR>(
					maxNumberOfCellsInSpan,
					binaryRulesArray,
					sentenceLexicalGenerators, 
					sloppyLexicalGenerators,
					categoryServices, 
					pruneLexicalCells, 
					completeParseFilter,
					unaryRulesArray,
					lexicalRule,
					breakTies
				);
		}

		public Builder<DI, MR> setBreakTies(boolean breakTies) {
			this.breakTies = breakTies;
			return this;
		}

		public Builder<DI, MR> setCompleteParseFilter(
				IFilter<Category<MR>> completeParseFilter) {
			this.completeParseFilter = completeParseFilter;
			return this;
		}

		public void setLexicalRule(ILexicalRule<MR> lexicalRule) {
			this.lexicalRule = lexicalRule;
		}

		public Builder<DI, MR> setMaxNumberOfCellsInSpan(
				int maxNumberOfCellsInSpan) {
			this.maxNumberOfCellsInSpan = maxNumberOfCellsInSpan;
			return this;
		}

		public Builder<DI, MR> setPruneLexicalCells(boolean pruneLexicalCells) {
			this.pruneLexicalCells = pruneLexicalCells;
			return this;
		}
	}
}
