package edu.cornell.cs.nlp.spf.parser.ccg.cky.bestfirst;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

import edu.cornell.cs.nlp.spf.parser.ccg.cky.chart.Cell;

/**
 * A ordered agenda for {@link Cell} instances
 * @author ethantkoenig
 
 */
public class Agenda<MR> {
	
	/**
	 * Queue of {@link Cell} instances currently in agenda
	 */
	private PriorityQueue<Cell<MR>> queue;
	
	/**
	 * Set of {@link Cell} instances that have already been added to the agenda
	 */
	private Set<Cell<MR>> seenCells = new HashSet<>();
	
	
	public Agenda(Comparator<Cell<MR>> comparator) {
		queue = new PriorityQueue<>(comparator);
	}
	
	/**
	 * Add {@code cell} to agenda
	 * @param cell {@link Cell} to add to agenda
	 */
	public void add(Cell<MR> cell) {
		if (!seenCells.contains(cell)) {
			seenCells.add(cell);
			queue.add(cell);
		}
	}
	
	
	/**
	 * Add all cells in {@code cells} to agenda
	 * @param cells {@link Cell} instances to add to agenda
	 */
	public void addAll(Collection<Cell<MR>> cells) {
		for(Cell<MR> cell : cells) {
			add(cell);
		}
	}
	
	/**
	 * Removes head of agenda, and returns it
	 * @return removed head of agenda, or {@code null} if agenda is empty
	 */
	public Cell<MR> poll() {
		return queue.poll();
	}
	
	
	/**
	 * @return whether agenda is empty
	 */
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	
	/**
	 * @return number of elements in agenda
	 */
	public int size() {
		return queue.size();
	}
	
	
	
	
	
	

}
